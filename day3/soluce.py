from pathlib import Path
import math

values = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'

p = Path(__file__).with_name('input')

def find_common_chat(first, second):
    for letter in first:
        if letter in second:
            return letter


with p.open('r') as f:
    total = 0
    for backpack in f:
        middle = math.ceil((len(backpack) - 1) / 2)
        common = find_common_chat(backpack[:middle], backpack[middle:])
        total += values.index(common) + 1

print(total)
