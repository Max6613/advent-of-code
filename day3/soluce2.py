from pathlib import Path
import math

values = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'

p = Path(__file__).with_name('input')

def find_common(backpacks):
    for letter in backpacks[0]:
        if letter in backpacks[1] and letter in backpacks[2]:
            return letter


with p.open('r') as f:
    total = 0
    backpacks = []
    for backpack in f:
        backpacks.append(backpack)
        if len(backpacks) >= 3:
            common = find_common(backpacks)
            backpacks = []
            total += values.index(common) + 1

print(total)
