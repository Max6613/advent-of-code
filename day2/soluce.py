from pathlib import Path

values = {
    'X': 1,
    'Y': 2,
    'Z': 3,
}

possibilities = {
    'A X': 3,
    'A Y': 6,
    'A Z': 0,
    'B X': 0,
    'B Y': 3,
    'B Z': 6,
    'C X': 6,
    'C Y': 0,
    'C Z': 3
}

p = Path(__file__).with_name('input')

with p.open('r') as f:
    total = sum(possibilities[fight[:-1]] + values[fight[2]] for fight in f)
print(total)