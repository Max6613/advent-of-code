from pathlib import Path

values = {
    'A': 1,
    'B': 2,
    'C': 3,
}


def find_move_to_loose(opponent):
    if opponent == 'A':
        return 'C'
    elif opponent == 'B':
        return 'A'
    elif opponent == 'C':
        return 'B'


def find_move_to_win(opponent):
    if opponent == 'A':
        return 'B'
    elif opponent == 'B':
        return 'C'
    elif opponent == 'C':
        return 'A'


p = Path(__file__).with_name('input')

with p.open('r') as f:
    total = 0
    for fight in f:
        result = fight[2]
        if result == 'X':  # Loose
            total += values[find_move_to_loose(fight[0])]
        elif result == 'Y':  # Draw
            total += values[fight[0]] + 3
        elif result == 'Z':  # Win
            total += values[find_move_to_win(fight[0])] + 6

print(total)
