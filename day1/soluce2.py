from pathlib import Path

p = Path(__file__).with_name('input.txt')

podium = [0, 0, 0]

with p.open('r') as f:
    lutin = 0
    for line in f:
        try:
            lutin += int(line)
        except ValueError:
            podium.append(lutin)
            podium.sort(reverse=True)
            podium = podium[:3]
            lutin = 0
print(sum(podium))