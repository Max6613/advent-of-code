from pathlib import Path

p = Path(__file__).with_name('input.txt')

with p.open('r') as f:
    better_lutin = None
    lutin = 0
    for line in f:
        try:
            lutin += int(line)
        except ValueError:
            if not better_lutin or lutin > better_lutin:
                better_lutin = lutin
            lutin = 0
print(better_lutin)