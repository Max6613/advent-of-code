from pathlib import Path
from pprint import pprint
NB_COLUMNS = 3
EMPTY_LINE = ['   '] * NB_COLUMNS


def generate_stack_line(line, n=4):
    return [line[i:i+n-1] for i in range(0, len(line), n)]


def extract_data_from_line(line):
    data = line.split('move ')[1].split(' from ')
    stacks = data[1].split(' to ')
    return {
        'crate_nb': int(data[0]),
        'stack_from': int(stacks[0]),
        'stack_to': int(stacks[1])
    }


def find_crates_to_move_crates(stacks, data):
    to_move = []
    for i, line in enumerate(stacks):
        column_index = data['stack_from'] - 1
        if line[column_index] == '   ' or i >= len(stacks) - 1:
            for j in range(1, data['crate_nb'] + 1):
                line_index = i - j 
                to_move.append(stacks[line_index][column_index])
                stacks[line_index][column_index] = '   '
            break
    return to_move


def move_crates(stacks, data, to_move):
    for i, line in enumerate(stacks):
        column_index = data['stack_to'] - 1
        if line[column_index] == '   ':
            for j in range(len(to_move)):
                line_index = i + j 
                if line_index > len(stacks) - 1:
                    stacks.append(EMPTY_LINE)
                stacks[line_index][column_index] = to_move.pop(0)
            break
    return stacks

p = Path(__file__).with_name('input')

movement_started = False
stacks = []

with p.open('r') as f:
    for line in f:
        line = line[:-1]
        # Creating started stack
        if not movement_started:
            if line == '':
                movement_started = True
            else:
                stacks.insert(0, generate_stack_line(line))
        # Moving crates
        else:
            data = extract_data_from_line(line)
            to_move = find_crates_to_move_crates(stacks, data)
            pprint(to_move)
            stacks = move_crates(stacks, data, to_move)
    pprint(stacks)