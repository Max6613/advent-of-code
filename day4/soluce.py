from pathlib import Path


def fully_contains(peers):
    range1 = [int(x) for x in peers[0].split('-')]
    range2 = [int(x) for x in peers[1].split('-')]

    if ((range1[0] >= range2[0] and range1[1] <= range2[1]) or
            (range2[0] >= range1[0] and range2[1] <= range1[1])):
        return True
    return False


p = Path(__file__).with_name('input')

with p.open('r') as f:
    total = 0
    for peer in f:
        peers = peer[:-1].split(',')
        if fully_contains(peers):
            total += 1
print(total)
