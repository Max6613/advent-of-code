from pathlib import Path


def does_overlap(peers):
    range1 = [int(x) for x in peers[0].split('-')]
    range2 = [int(x) for x in peers[1].split('-')]

    x = range1[0]
    y = range1[1]
    a = range2[0]
    b = range2[1]
    return ((x >= a and x <= b) or (y >= a and y <= b)
            or (a >= x and a <= y) or (b >= x and b <= y))



p = Path(__file__).with_name('input')

with p.open('r') as f:
    total = 0
    for peer in f:
        peers = peer[:-1].split(',')
        if does_overlap(peers):
            total += 1
print(total)
